#ifndef BMP_FORMAT
#define BMP_FORMAT
#include "io_status.h"
#include "stdio.h"
#include "structures.h"

enum read_status from_bmp(FILE* const file, struct image* const image);

enum write_status to_bmp(FILE* const file, const struct image * image);

#endif
