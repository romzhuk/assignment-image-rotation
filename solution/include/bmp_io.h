#ifndef BMP_IO
#define BMP_IO
#include "bmp_format.h"
#include "utils.h"

enum read_status read_bmp(const char* path, struct image* image);

enum write_status write_bmp(const char* path, const struct image* image);

#endif
