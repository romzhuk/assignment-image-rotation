#ifndef ERROR_MESSAGE
#define ERROR_MESSAGE
#include "io_status.h"

void print_read_error(enum read_status status);

#endif
