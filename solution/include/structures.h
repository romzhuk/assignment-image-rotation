#ifndef IMAGE
#define IMAGE
#include "stdint.h"

#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)

#pragma pack(push, 1)
struct image {
    uint64_t width, height;
    struct pixel* data;
};
#pragma pack(pop)

#endif
