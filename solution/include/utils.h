#ifndef UTILS
#define UTILS
#include "io_status.h"
#include "stdio.h"
#include  "structures.h"

enum read_status read_from_path(const char* path, struct image* image, enum read_status (*f)(FILE*, struct image*));

enum write_status write_to_path(const char* path, const struct image* image, enum write_status (*f)(FILE*, const struct image*));

void init_image(struct image* image, uint64_t width, uint64_t height);

void free_image(struct image image);

size_t calc_pixel_pos(uint64_t width, uint64_t x, uint64_t y);

#endif
