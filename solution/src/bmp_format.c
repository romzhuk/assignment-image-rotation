#include "../include/bmp_format.h"
#include "../include/io_status.h"
#include  "../include/structures.h"
#include "../include/utils.h"
#include "inttypes.h"
#include "stdio.h"
#include "stdlib.h"
#define PADDING_BYTES 4
#define TYPE 0x4D42
#define BF_RESERVED 0
#define BI_SIZE 40
#define BI_PLANES 1
#define BI_BIT_COUNT 24
#define BI_COMPRESSION 0
#define BI_X_PPM 0
#define BI_Y_PPM 0
#define BI_CLR_USED 0
#define BI_CLR_IMPORTANT 0
#define ATTRIBUTES_NUM 15

struct __attribute__((packed)) bmp_header 
{
  uint16_t bfType;
  uint32_t bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
};

static uint8_t padding_calc(uint32_t width) {
  return (PADDING_BYTES - sizeof(struct pixel) * width % PADDING_BYTES) % PADDING_BYTES;
}

static uint8_t padding_header_calc(const struct bmp_header header) {
  return padding_calc(header.biWidth);
}

static enum read_status read_check(const struct bmp_header header) {
  if(header.bfType != TYPE) return READ_INVALID_SIGNATURE;
  else if(header.biPlanes != BI_PLANES) return READ_INVALID_PLANES;
  else if(header.biBitCount != BI_BIT_COUNT) return READ_INVALID_BITS;
  else if(header.biCompression != BI_COMPRESSION) return READ_INVALID_COMPRESSION;
  return READ_OK;
}

static enum read_status read_header(FILE* file, struct bmp_header* header) {
  return fread(header, sizeof(struct bmp_header), 1, file) == 1 ? READ_OK : READ_HEADER_ERROR;
}

static enum read_status read_pixels(FILE* const file, const struct bmp_header header, struct image* const image) {
  init_image(image, header.biWidth, header.biHeight);
  const uint8_t padding = padding_header_calc(header);
  if(fseek(file, header.bOffBits, SEEK_SET)) return READ_PIXELS_ERROR;
  for(size_t i = 0; i < image->height; i++) {
    if(fread(((image->data) + i * image->width), sizeof(struct pixel), image->width, file) != image->width) return READ_PIXELS_ERROR;
    if(fseek(file, padding, SEEK_CUR)) return READ_PIXELS_ERROR;
  }
  return READ_OK;
}

enum read_status from_bmp(FILE* const file, struct image* const image) {
  struct bmp_header header = {0};
  if(read_header(file, &header) == READ_HEADER_ERROR) return READ_HEADER_ERROR;
  if(read_check(header) == READ_OK) return read_pixels(file, header, image);
  return read_check(header);
}

static struct bmp_header generate_header(const struct image image) {
  struct bmp_header header = {0};
  const uint8_t padding = padding_calc(image.width);
  header.bfType = TYPE;
  header.bfileSize = (sizeof(struct bmp_header) + (image.width + padding) * image.height * sizeof(struct pixel));
  header.bfReserved = BF_RESERVED;
  header.bOffBits = sizeof(struct bmp_header);
  header.biSize = BI_SIZE;
  header.biWidth = image.width;
  header.biHeight = image.height;
  header.biPlanes = BI_PLANES;
  header.biBitCount = BI_BIT_COUNT;
  header.biCompression = BI_COMPRESSION;
  header.biSizeImage = (image.width) * image.height * sizeof(struct pixel);
  header.biXPelsPerMeter = BI_X_PPM;
  header.biYPelsPerMeter = BI_Y_PPM;
  header.biClrUsed = BI_CLR_USED;
  header.biClrImportant = BI_CLR_IMPORTANT;
  return header;
}

static enum write_status write_pixels(FILE* const file, const struct image image) {
  const uint8_t padding = padding_calc(image.width);
  const uint8_t void_byte = 0;
  for(size_t i = 0; i < image.height; i++) {
    if(fwrite(image.data + i * image.width, sizeof(struct pixel), image.width, file) != image.width) return WRITE_ERROR;
    for(size_t t = 0; t < (size_t)padding; t++) if(fwrite(&void_byte, sizeof(uint8_t), 1, file) != 1) return WRITE_ERROR;
  }
  return WRITE_OK;
}

static enum write_status write_header(FILE* const file, const struct bmp_header header) {
  return fwrite(&header, sizeof(struct bmp_header), 1, file) == 1 ? WRITE_OK : WRITE_ERROR;
}

enum write_status to_bmp(FILE* const file, const struct image * image) {
  struct bmp_header header = generate_header(*image);
  enum write_status header_status = write_header(file, header);
  enum write_status pixels_status = write_pixels(file, *image);
  if(header_status == WRITE_OK && pixels_status == WRITE_OK) return WRITE_OK;
  return WRITE_ERROR;
}
