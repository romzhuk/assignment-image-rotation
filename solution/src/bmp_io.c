#include "../include/bmp_format.h"
#include "../include/bmp_io.h"
#include "../include/utils.h"

enum read_status read_bmp(const char* path, struct image* image) {
    return read_from_path(path, image, from_bmp);
}

enum write_status write_bmp(const char* path, const struct image* image) {
    return write_to_path(path, image, to_bmp);
}
