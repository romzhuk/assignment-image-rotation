#include "../include/error_message.h"
#include "stdio.h"

void print_read_error(enum read_status status) {
    const char *messages[] = {"Image was read successfully\n", "Invalid file type\n", 
    "Invalid number of planes\n", "Invalid number of header bits\n", "Image is compressed\n",
    "Invalid file and/or image pointer\n", "Something went wrong during reading of pixels\n",
    "Something went wrong during reading of header\n"};
    fprintf(stderr, "%s", messages[status]);
}
