#include "../include/bmp_io.h"
#include "../include/error_message.h"
#include "../include/rotation_transformation.h"

int main( int argc, char** argv ) {
    if(argc < 3) {
        fprintf(stderr, "Not enough arguments\n");
        return 1;
    }
    struct image image = {0};
    enum read_status r_status = read_bmp(argv[1], &image);
    print_read_error(r_status); 
    if(r_status != READ_OK) {
        return 1;
    }
    struct image rotated = rotate(image);
    if(write_bmp(argv[2], &rotated) != WRITE_OK) {
        fprintf(stderr, "Failed to write resulting image\n");
        return 1;
    }
    free_image(image);
    free_image(rotated);
    printf("Image was rotated successfully\n");
    return 0;
}
