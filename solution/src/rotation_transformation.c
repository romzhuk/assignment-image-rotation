#include "../include/rotation_transformation.h"
#include  "../include/structures.h"
#include "../include/utils.h"
#include "inttypes.h"
#include "stdint.h"
#include "stdlib.h"

static size_t calc_rotated_pos(uint64_t height, uint64_t x, uint64_t y) {
    return (x - 1) * height + (height - (y - 1) - 1);
}

struct image rotate(const struct image image) {
    struct image rotated;
    rotated.height = image.width;
    rotated.width = image.height;
    rotated.data = malloc(sizeof(struct pixel) * rotated.width * rotated.height);
    for(size_t i = 0; i < image.width; i++) {
        for(size_t t = 0; t < image.height; t++) {
            rotated.data[calc_rotated_pos(image.height, i + 1, t + 1)] = image.data[calc_pixel_pos(image.width, i + 1, t + 1)];
        }
    }
    return rotated;
}
