#include "../include/utils.h"
#include "../include/io_status.h"
#include  "../include/structures.h"
#include "stdio.h"
#include "stdlib.h"

enum read_status read_from_path(const char* path, struct image* image, enum read_status (*f)(FILE*, struct image*)) {
    FILE* file = fopen(path, "r+b");
    enum read_status result = READ_INVALID_POINTER;
    if(file && image) result = f(file, image);
    else result = READ_INVALID_POINTER;
    if(file) fclose(file);
    return result;
}

enum write_status write_to_path(const char* path, const struct image* image, enum write_status (*f)(FILE*, const struct image*)) {
    FILE* file2;
    enum write_status result = WRITE_ERROR;
    if ((file2= fopen(path, "wb"))!=NULL)
    {
        if(file2 && image) result = f(file2, image);
        else result =  WRITE_ERROR;
        fclose(file2);
    }
    return result;
}

void init_image(struct image* image, uint64_t width, uint64_t height) {
    image->width = width;
    image->height = height;
    image->data = malloc(sizeof(struct pixel) * image->height * image->width);
}

void free_image(struct image image) {
    if(image.data) free(image.data);
}

size_t calc_pixel_pos(uint64_t width, uint64_t x, uint64_t y) {
    return (y - 1) * width + (x - 1);
}
